class Api::V1::DelegatesController < ApplicationController
  skip_before_action :verify_authenticity_token
  # before_filter :fetch_delegate, :except => [:index]

  def fetch_delegate
    @delegate = Delegate.find_by_id(params[:id])
  end

  def index
    @delegates = Delegate.all
    respond_to do |format|
      format.json { render json: @delegates }
      format.xml { render xml: @delegates }
    end
  end

  def new
  end

  def edit
  end

  def show
    # respond_to do |format|
    #   format.json { render json: @delegate }
    #   format.xml { render xml: @delegate }
    # end
  end
end

