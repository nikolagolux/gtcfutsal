class Api::V1::FoulsController < ApplicationController
  before_action :set_article, only: [:show, :edit, :update, :destroy]
  skip_before_filter :authenticate_user!
  skip_before_action :verify_authenticity_token

  def index
    @fouls = Foul.all
    respond_to do |format|
      format.json {render json: @fouls}
      format.xml {render xml: @fouls}
    end 
  end

  def new
    # @foul = Foul.new
    # @player_seasons = PlayerSeason.all
    # @player = Player.where(:id => :player_id)
  end

  def new_home
    @foul = Foul.new
    # Ovde u @player_season treba da oznacimo sve igrace iz HOME tima
    #@player = Player.where(:id => :player_id)
    # Ovde u @player_season treba da oznacimo sve igrace iz HOME tima
    @home_team = current_match.home_team.team
    @players = Player.where(:team_id => @home_team.id)
    # Ovde trazimo player_sezone
    i = 0
    @players.each do |player|
      @player_season = PlayerSeason.where(:player_id => player.id)
      if i == 0 then
        @player_seasons = @player_season
      else 
        @player_seasons.concat(@player_season)
      end
      i = 1
    end
    session[:is_home] = true
    # U promenljivu match_players postavljamo igrace
    # koji su izabrani da su dosli na utakmicu
    @match_players = MatchPlayer.where(
      :match_id => current_match.id,
      :is_home => session[:is_home])

    # Ovde trazimo player_sezone za izabrane igrace,
    # glavna promenljiva je player_seasons_on_match
    i = 0
    @match_players.each do |match_player|
      @player_season_on_match = PlayerSeason.where(:id => match_player.player_season_id)
      if i == 0 then
        @player_seasons_on_match = @player_season_on_match
      else
        @player_seasons_on_match.concat(@player_season_on_match)
      end
      i = 1
    end
  end

  def new_away
    @foul = Foul.new
    # Ovde u @player_season treba da oznacimo sve igrace iz AWAY tima
    # @team = Team.find() ...
    # @player = Player.where(:team_id => self.team)
    @away_team = current_match.away_team.team
    @players = Player.where(:team_id => @away_team.id)
    # Ovde trazimo player_sezone
    i = 0
    @players.each do |player|
      @player_season = PlayerSeason.where(:player_id => player.id)
      if i == 0 then
        @player_seasons = @player_season
      else 
        @player_seasons.concat(@player_season)
      end
      i = 1
    end
    session[:is_home] = false
    # U promenljivu match_players postavljamo igrace
    # koji su izabrani da su dosli na utakmicu
    @match_players = MatchPlayer.where(
      :match_id => current_match.id,
      :is_home => session[:is_home])

    # Ovde trazimo player_sezone za izabrane igrace,
    # glavna promenljiva je player_seasons_on_match
    i = 0
    @match_players.each do |match_player|
      @player_season_on_match = PlayerSeason.where(:id => match_player.player_season_id)
      if i == 0 then
        @player_seasons_on_match = @player_season_on_match
      else
        @player_seasons_on_match.concat(@player_season_on_match)
      end
      i = 1
    end 
  end

  def create
    #************************************************************
    # Faulovi ne mogu da se kreiraju ako match nije startovan
    #************************************************************
     @foul = Foul.new(foul_params)
     # Dodeljujemo datom faul da je dat na odredjenoj utakmici
     @current_match = Match.find_by(:id => @foul.match_id)
     # Pitamo se da li je faul dao domacin ili gost
     @pauza = (@current_match.second_half_started - @current_match.first_half_ended)
     @foul_scored_m = (DateTime.now.seconds_since_midnight - @pauza - @current_match.match_started).to_f / 60

     @foul.created_at = @foul_scored_m.round

     
     respond_to do |format|
      if @foul.save
        format.json { render json: @foul, status: :created }
        format.xml { render xml: @foul, status: :created }
      else
        format.json { render json: @foul, status: :unprocessable_entity }
        format.xml { render xml: @foul, status: :unprocessable_entity }
      end
    end
  end

  def edit
  end

  def update
  end

  def show
    respond_to do |format|
      format.json { render json: @foul }
      format.xml { render xml: @foul }
    end
  end

  private

   def set_article
    @foul = Foul.find(params[:id])
   end

  def foul_params
    params.require(:foul).permit( :match_id, :player_season_id, :is_home)
  end

end
