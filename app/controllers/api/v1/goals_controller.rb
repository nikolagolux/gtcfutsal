class Api::V1::GoalsController < ApplicationController
  before_action :set_goal, only: [:show, :edit, :update, :destroy]
  skip_before_filter :authenticate_user! # we do not need devise authentication here
  skip_before_action :verify_authenticity_token

  def index
    @goals = Goal.all
    respond_to do |format|
      format.json { render json: @goals }
      format.xml { render xml: @goals }
    end
  end

  def new
    # @goal = Goal.new
    # @player_seasons = PlayerSeason.all
    # @player = Player.where(:id => :player_id)
  end

  def new_home
    @goal = Goal.new
    # Ovde u @player_season treba da oznacimo sve igrace iz HOME tima
    @home_team = @current_match.home_team.team
    @players = Player.where(:team_id => @home_team.id)
    # Ovde trazimo player_sezone
    i = 0
    @players.each do |player|
      @player_season = PlayerSeason.where(:player_id => player.id)
      if i == 0 then
        @player_seasons = @player_season
      else 
        @player_seasons.concat(@player_season)
      end
      i = 1
    end
    session[:is_home] = true
    # U promenljivu match_players postavljamo igrace
    # koji su izabrani da su dosli na utakmicu
    @match_players = MatchPlayer.where(
      :match_id => @current_match.id,
      :is_home => session[:is_home])

    # Ovde trazimo player_sezone za izabrane igrace,
    # glavna promenljiva je player_seasons_on_match
    i = 0
    @match_players.each do |match_player|
      @player_season_on_match = PlayerSeason.where(:id => match_player.player_season_id)
      if i == 0 then
        @player_seasons_on_match = @player_season_on_match
      else
        @player_seasons_on_match.concat(@player_season_on_match)
      end
      i = 1
    end
  end

  def new_away
    @goal = Goal.new
    # Ovde u @player_season treba da oznacimo sve igrace iz AWAY tima
    @away_team = @current_match.away_team.team
    @players = Player.where(:team_id => @away_team.id)
    # Ovde trazimo player_sezone
    i = 0
    @players.each do |player|
      @player_season = PlayerSeason.where(:player_id => player.id)
      if i == 0 then
        @player_seasons = @player_season
      else
        @player_seasons.concat(@player_season)
      end
      i = 1
    end
    #@player_seasons = PlayerSeason.where(:player_id => )
    session[:is_home] = false
    # U promenljivu match_players postavljamo igrace
    # koji su izabrani da su dosli na utakmicu
    @match_players = MatchPlayer.where(
      :match_id => @current_match.id,
      :is_home => session[:is_home])

    # Ovde trazimo player_sezone za izabrane igrace,
    # glavna promenljiva je player_seasons_on_match
    i = 0
    @match_players.each do |match_player|
      @player_season_on_match = PlayerSeason.where(:id => match_player.player_season_id)
      if i == 0 then
        @player_seasons_on_match = @player_season_on_match
      else
        @player_seasons_on_match.concat(@player_season_on_match)
      end
      i = 1
    end
  end

  def create
    @goal = Goal.new(goal_params)
    # Dodeljujemo datom golu da je dat na odredjenoj utakmici
    @current_match = Match.find_by(:id => @goal.match_id)
    # Da bi nasli u kom minutu je dat gol,
    # prvo gledamo u kojoj sekundi, pa delimo sa 60 da bi dobili minute
    # takodje, pre svega, racunamo koliko je trajala pauza na poluvremenu i oduzimamo tu pauzu
    # POSTOJI MOGUCNOST DA SE DODA DUGME STOP I CONTINUE MATCH...
    @pauza = (@current_match.second_half_started - @current_match.first_half_ended)
    @goal_scored_m = (DateTime.now.seconds_since_midnight - @pauza - @current_match.match_started).to_f / 60
    # U kom minutu je dat gol? Zaokruzujemo metodom .round
    @goal.created_at = @goal_scored_m.round

    # Pitamo se da li je gol dao domacin ili gost
    # @is_goal_home = session[:is_home]

    # Bodovanje igraca se vrsi po vaznosti utakmice V i jacini ekipe J
    # Bodovanje igraca je zbir Q+W+E+R+T
    # Dodajemo bodove datom igracu za postignut gol (Q)
    # Prvo trazimo igraca koji je dao gol
    @player_season = PlayerSeason.find_by(:id => @goal.player_season_id)

    #*************************************************
    # Dodajemo bedz za brzi gol
    #*************************************************
    if @goal.created_at < 4
      @player_badge = PlayerBadge.find_by(:player_id => @player_season.player.id)
      @broj_brzih_golova = @player_badge.gol_u_prva_2_min
      @broj_brzih_golova += 1
      @player_badge.update(:gol_u_prva_2_min => @broj_brzih_golova)
      #*************************************************
      # Dodajemo EXP za bedz za brzi gol
      #*************************************************
      @exp_za_brz_gol = @player_season.expirience + 250
      @player_season.update(:expirience => @exp_za_brz_gol)
    end
    # Zatim gledamo da li je mec 1) prijateljski; 2) drugoligaski, 3) kup, 4) prvoligaski
    # 1) prijateljski mec, V = 1
    if @current_match.is_friendly
      @v = 1
    # 2) drugoligaski mec, V = 2.5
    elsif @current_match.is_second_league
      @v = 2.5
    # 3) kup, V = 3.0
    elsif @current_match.is_tournament
      @v = 3.0
    # 4) prvoligaski mec, V = 4.0
    elsif @current_match.is_first_league
      @v = 4.0
    end

      # Trazimo ukupan broj 5+1 ekipa
    # i ukupan broj 4+1 ekipa
    @svi_timovi = Team.all
    # Ovde trazimo team_sezone
    i = 0
    j = 0
    @svi_timovi.each do |team|
      if team.league != nil
        if team.league.is_fiveplusone == true
          @team_season_5_1 = TeamSeason.where(:team_id => team.id)
          if i == 0 then
            @team_seasons_5_1 = @team_season_5_1
          else 
            @team_seasons_5_1.concat(@team_season_5_1)
          end
          i = 1
        elsif team.league.is_fourplusone == true
          @team_season_4_1 = TeamSeason.where(:team_id => team.id)
          if j == 0 then
            @team_seasons_4_1 = @team_season_4_1
          else 
            @team_seasons_4_1.concat(@team_season_4_1)
          end
          j = 1
        end
      end
    end

    # Broj 5+1 ekipa
    @ukupno_ekipa_5_1 = @team_seasons_5_1.count

    # Broj 4+1 ekipa
    if @team_seasons_4_1 != nil
      @ukupno_ekipa_4_1 = @team_seasons_4_1.count
    else
      @ukupno_ekipa_4_1 = 0
    end

    # Minimalna jacina tima
    @minimalna_jacina_tima_5_1 = 15
    @minimalna_jacina_tima_4_1 = 15

    ###########################
    # Jacina protivnickog tima
    ###########################
    # Definisan home team
    @home_team = @current_match.home_team.team
    @home_team_season = @home_team.team_season
    # Definisan away team
    @away_team = @current_match.away_team.team
    @away_team_season = @away_team.team_season
    #############################
    # Racunamo jacinu HOME tima
    #############################
    # Za prvoplasiranu i poslednjih 15 ekipa postoje drugi kriterijumi za bodovanje
    # LIGA 4+1
    if @home_team.league.is_fourplusone
      if @home_team.team_season.position == nil || @home_team.team_season.position == 0
        @home_team.team_season.position = 0
        @jacina_domaceg_tima = @minimalna_jacina_tima_4_1
      # Ako je protivnicki tim prvoplasiran dobija maksimalan broj bodova
      elsif @home_team.team_season.position == 1
        @jacina_domaceg_tima = @ukupno_ekipa_4_1
      # Standardna formula za racunanje bodova
      elsif @home_team.team_season.position > 1 && @home_team.team_season.position < (@ukupno_ekipa_4_1 - 14)
        @jacina_domaceg_tima = @ukupno_ekipa_4_1 - @home_team.team_season.position
      # Ako je protivnicki tim u poslednjih petnaest
      elsif @home_team.team_season.position > (@ukupno_ekipa_4_1 - 15)
        @jacina_domaceg_tima = @minimalna_jacina_tima_4_1
      end
    # LIGA 5+1
    elsif @home_team.league.is_fiveplusone
      if @home_team.team_season.position == nil || @home_team.team_season.position == 0
        @home_team.team_season.position = 0
        @jacina_domaceg_tima = @minimalna_jacina_tima_5_1
      # Ako je protivnicki tim prvoplasiran dobija maksimalan broj bodova
      elsif @home_team.team_season.position == 1
        @jacina_domaceg_tima = @ukupno_ekipa_5_1
      # Standardna formula za racunanje bodova
      elsif @home_team.team_season.position > 1 && @home_team.team_season.position < (@ukupno_ekipa_5_1 - 14)
        @jacina_domaceg_tima = @ukupno_ekipa_5_1 - @home_team.team_season.position
      # Ako je protivnicki tim u poslednjih petnaest
      elsif @home_team.team_season.position > (@ukupno_ekipa_5_1 - 15)
        @jacina_domaceg_tima = @minimalna_jacina_tima_5_1
      end
    end
    @j = @jacina_domaceg_tima

    #############################
    # Racunamo jacinu AWAY tima
    #############################
        # Za prvoplasiranu i poslednjih 15 ekipa postoje drugi kriterijumi za bodovanje
    # LIGA 4+1
    if @away_team.league.is_fourplusone
      if @away_team.team_season.position == nil || @away_team.team_season.position == 0
        @away_team.team_season.position = 0
        @jacina_gostujuceg_tima = @minimalna_jacina_tima_4_1
      # Ako je protivnicki tim prvoplasiran dobija maksimalan broj bodova
      elsif @away_team.team_season.position == 1
        @jacina_gostujuceg_tima = @ukupno_ekipa_4_1
      # Standardna formula za racunanje bodova
      elsif @away_team.team_season.position > 1 && @away_team.team_season.position < (@ukupno_ekipa_4_1 - 14)
        @jacina_gostujuceg_tima = @ukupno_ekipa_4_1 - @away_team.team_season.position
      # Ako je protivnicki tim u poslednjih petnaest
      elsif @away_team.team_season.position > (@ukupno_ekipa_4_1 - 15)
        @jacina_gostujuceg_tima = @minimalna_jacina_tima_4_1
      end
    # LIGA 5+1
    elsif @away_team.league.is_fiveplusone
      if @away_team.team_season.position == nil  || @away_team.team_season.position == 0
        @away_team.team_season.position = 0
        @jacina_gostujuceg_tima = @minimalna_jacina_tima_5_1
      # Ako je protivnicki tim prvoplasiran dobija maksimalan broj bodova
      elsif @away_team.team_season.position == 1
        @jacina_gostujuceg_tima = @ukupno_ekipa_5_1
      # Standardna formula za racunanje bodova
      elsif @away_team.team_season.position > 1 && @away_team.team_season.position < (@ukupno_ekipa_5_1 - 14)
        @jacina_gostujuceg_tima = @ukupno_ekipa_5_1 - @away_team.team_season.position
      # Ako je protivnicki tim u poslednjih petnaest
      elsif @away_team.team_season.position > (@ukupno_ekipa_5_1 - 15)
        @jacina_gostujuceg_tima = @minimalna_jacina_tima_5_1
      end
    end
    @j = @jacina_gostujuceg_tima

    # Bodovi za gol se proracunavaju po formuli 1*V*J*3 (prva jedinica predstavlja broj golova - BG)
    @bodovi_za_gol = @v*@j*3
    @bodovi_za_gol.round
    @bodovi_za_gol += @player_season.expirience
    @player_season.update(:expirience => @bodovi_za_gol)

    # Odredjivanje levela igraca u odnosu na osvojene bodove
    if @player_season.expirience < 251
      @level = 1
    elsif @player_season.expirience > 250 && @player_season.expirience < 1501
      @level = 2
    elsif @player_season.expirience > 1500 && @player_season.expirience < 3501
      @level = 3
    elsif @player_season.expirience > 3500 && @player_season.expirience < 6001
      @level = 4
    elsif @player_season.expirience > 6000 && @player_season.expirience < 10001
      @level = 5
    elsif @player_season.expirience > 10000 && @player_season.expirience < 14001
      @level = 6
    elsif @player_season.expirience > 14000 && @player_season.expirience < 18001
      @level = 7
    elsif @player_season.expirience > 18000 && @player_season.expirience < 22001
      @level = 8
    elsif @player_season.expirience > 22000 && @player_season.expirience < 26001
      @level = 9
    elsif @player_season.expirience > 26000 && @player_season.expirience < 30001
      @level = 10
    elsif @player_season.expirience > 30000 && @player_season.expirience < 34001
      @level = 11
    elsif @player_season.expirience > 34000 && @player_season.expirience < 38001
      @level = 12
    elsif @player_season.expirience > 38000 && @player_season.expirience < 42001
      @level = 13
    elsif @player_season.expirience > 42000 && @player_season.expirience < 46001
      @level = 14
    elsif @player_season.expirience > 46000 && @player_season.expirience < 50001
      @level = 15
    elsif @player_season.expirience > 50000 && @player_season.expirience < 54001
      @level = 16
    elsif @player_season.expirience > 54000 && @player_season.expirience < 58001
      @level = 17
    elsif @player_season.expirience > 58000 && @player_season.expirience < 62001
      @level = 18
    elsif @player_season.expirience > 62000 && @player_season.expirience < 66001
      @level = 19
    elsif @player_season.expirience > 66000 && @player_season.expirience < 70001
      @level = 20
    elsif @player_season.expirience > 70000 && @player_season.expirience < 74001
      @level = 21
    elsif @player_season.expirience > 74000 && @player_season.expirience < 78001
      @level = 22
    elsif @player_season.expirience > 78000 && @player_season.expirience < 82001
      @level = 23
    elsif @player_season.expirience > 82000 && @player_season.expirience < 86001
      @level = 24
    elsif @player_season.expirience > 86000 && @player_season.expirience < 90001
      @level = 25
    end

    @player_season.update(:level => @level)

      #*********************
      # Exp za bedz prvi gol
      #*********************
      if @player_season[:goals] == 0
        @exp_za_prvi_gol = @player_season.expirience + 100
        @player_season.update(:expirience => @exp_za_prvi_gol)
      end
      #************************
    # Ovde treba u odnosu na to u kom minutu je postignut gol,
    # da napravimo logiku za bedzeve (goals_in_chains itd.)
    respond_to do |format|
      if @goal.save
        format.json { render json: @goal, status: :created }
        format.xml { render xml: @goal, status: :created }
      else
        format.json { render json: @goal.errors, status: :unprocessable_entity }
        format.xml { render xml: @goal.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit
  end

  def update
  end

  def show
    respond_to do |format|
      format.json { render json: @goal }
      format.xml { render xml: @goal }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_goal
    @goal = Goal.find(params[:id])
  end

  def goal_params
    params.require(:goal).permit( :match_id, :player_season_id, :is_home)
  end

end
