class Api::V1::HomeTeamsController < ApplicationController
  skip_before_filter :authenticate_user! # we do not need devise authentication here
  skip_before_action :verify_authenticity_token

  def index
  	@home_teams = HomeTeam.all
  	respond_to do |format|
      format.json { render json: @home_teams }
      format.xml { render xml: @home_teams }
    end
  end

  def new
  end

  def edit
  end

  def show
    respond_to do |format|
      format.json { render json: @home_team }
      format.xml { render xml: @home_team }
    end
  end

  private
  def set_home_team
    @home_team = HomeTeam.find(params[:id])
  end
end
