class Api::V1::LeaguesController < ApplicationController
  before_action :set_league, only:[:edit, :update, :show]
  skip_before_filter :authenticate_user! # we do not need devise authentication here
  skip_before_action :verify_authenticity_token
  
  def index
    @leagues = League.all
    respond_to do |format|
      format.json {render json: @leagues}
      format.xml {render xml: @leagues}
    end  
  end

  def fourplusone
    @four_plus_one_leagues = League.where(:is_fourplusone => "1")
  end

  def fiveplusone
    @five_plus_one_leagues = League.where(:is_fiveplusone => "1")
  end

  def new
    @league = League.new
  end

  def create
   @league = League.new(league_params)
    if @league.save
      flash[:success] = "Welcome #{@league.name}"
      redirect_to leagues_path
    else
      render 'new'
    end
  end

  def edit
  end

  def update
  end

  # Tabela (Standardna tabela po ligama, bez EXPIRIENECE)
  def show 
    @teams = Team.where(:league_id => @league.id)
    if @teams
      # Ovde trazimo team_sezone
      i = 0
      @teams.each do |team|
        @team_season = TeamSeason.where(:team_id => team.id)
        if i == 0 then
          @team_seasons = @team_season
        else 
          @team_seasons.concat(@team_season)
        end
        i = 1
      end
    end

    if @team_seasons
      # Logika za sortiranje timova
      pozicija = 1
      @team_seasons.sort_by{|e| -e[:points]}.each do |team_season|
        team_season.update(:position => pozicija)
        pozicija += 1
      end
    end
    respond_to do |format|
      format.json {render json: @teams}
      format.xml {render xml: @teams}
    end
  end

  private
  def set_league
    @league = League.find(params[:id])
  end

  def league_params
    params.require(:league).permit(:name)
  end
end
