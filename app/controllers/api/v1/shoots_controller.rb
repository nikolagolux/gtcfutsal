class Api::V1::ShootsController < ApplicationController
  before_action :set_article, only: [:show, :edit, :update, :destroy]
  skip_before_filter :authenticate_user! # we do not need devise authentication here
  skip_before_action :verify_authenticity_token

	def index
		@shoots = Shoot.all
		respond_to do |format|
	      format.json {render json: @shoots}
	      format.xml {render xml: @shoots}
    	end
	end

	def new_home
		#************************************************************
	    # Sutevi ne mogu da se kreiraju ako match nije startovan
	    #************************************************************
		@shoot = Shoot.new(shoot_params)

		#@home_team_season = TeamSeason.where(:team_id => @home_team.id)
	    
	    @current_match = Match.find_by(:id => @shoot.match_id)

		# Pitamo se da li je faul dao domacin ili gost


		respond_to do |format|
	      if @shoot.save
	        format.json { render json: @shoot, status: :created }
	        format.xml { render xml: @shoot, status: :created }
	      else
	        format.json { render json: @shoot.errors, status: :unprocessable_entity }
	        format.xml { render xml: @shoot.errors, status: :unprocessable_entity }
	      end
	    end
	end

	def new_away
		#************************************************************
	    # Sutevi ne mogu da se kreiraju ako match nije startovan
	    #************************************************************
			@shoot = Shoot.new(shoot_params)

			#@away_team_season = TeamSeason.where(:team_id => @away_team.id)

			@current_match = Match.find_by(:id => @shoot.match_id)

			# Pitamo se da li je faul dao domacin ili gost

		respond_to do |format|
		    if @shoot.save
		     format.json { render json: @shoot, status: :created }
        	 format.xml { render xml: @shoot, status: :created }
		    else
		      format.json { render json: @shoot.errors, status: :unprocessable_entity }
        	  format.xml { render xml: @shoot.errors, status: :unprocessable_entity }
		    end
		end
	end

	def show
		respond_to do |format|
	      format.json { render json: @shoot }
	      format.xml { render xml: @shoot }
	    end
	end

	private 

	def set_article
		@shoot = Shoot.find(params[:id])
	end

	def shoot_params
		params.require(:shoot).permit(:match_id, :team_season_id, :is_home)
	end
end