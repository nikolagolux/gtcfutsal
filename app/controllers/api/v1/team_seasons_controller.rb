class Api::V1::TeamSeasonsController < ApplicationController
  skip_before_filter :authenticate_user! # we do not need devise authentication here
  skip_before_action :verify_authenticity_token
  before_filter :fetch_team_season, :except => [:index]

  def fetch_team_season
    @team_season = TeamSeason.find_by_id(params[:id])
  end

  def index
    @team_seasons = TeamSeason.all
    respond_to do |format|
      format.json { render json: @team_seasons }
      format.xml { render xml: @team_seasons }
    end
  end

  def new
  end

  def edit
  end

  def show
    respond_to do |format|
      format.json { render json: @team_season }
      format.xml { render xml: @team_season }
    end
  end
end



 


  
    