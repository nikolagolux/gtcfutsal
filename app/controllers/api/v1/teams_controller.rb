class Api::V1::TeamsController < ApplicationController
  skip_before_filter :authenticate_user! # we do not need devise authentication here
  skip_before_action :verify_authenticity_token
  before_filter :fetch_team, :except => [:index]


  def fetch_team
    @team = Team.find_by_id(params[:id])
  end

  def index
    @teams = Team.all
    respond_to do |format|
      format.json { render json: @teams }
      format.xml { render xml: @teams }
    end
  end

  def new
  end

  def edit
  end

  def show
    respond_to do |format|
      format.json { render json: @team }
      format.xml { render xml: @team }
    end
  end
end
