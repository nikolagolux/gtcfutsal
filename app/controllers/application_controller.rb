class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # protect_from_forgery with: :exception
  # For APIs, you may want to use :null_session instead.
  # Metode imaju uticaja na svim stranama
  before_action :leagues_nav, :tournaments_nav, :organisation_info, :language

  helper_method :logged_in?, :current_match#, :current_home, :current_away
  # :current_user, 
  ############################################
  # Ova komanda je za sistem za razmenu poruka 
  ############################################

  # Ukoliko ne pronadjemo neki ActiveRecord vrati nas na
  # root stranu
  rescue_from ActiveRecord::RecordNotFound do
    flash[:warning] = 'Resource not found.'
    redirect_back_or root_path
  end

  # Pravo da udje u Rails Admin-a ima samo admin
  # Odkomentarisati kasnije kad se zavrsi razvoj
  RailsAdmin.config do |config|
    config.authorize_with do
      redirect_to main_app.root_path flash[:error] = "You don't have permissions!" unless (current_user.admin)
    end
  end

  # Potrebno za autentifikaciju putem JSON-a
  #before_action :configure_permitted_parameters, if: :devise_controller?

  ###############################################
  after_filter :cors_set_access_control_headers, :cors_preflight_check

  # For all responses in this controller, return the CORS access control headers.

  def cors_set_access_control_headers
    headers['Access-Control-Allow-Origin'] = '*'
    headers['Access-Control-Allow-Methods'] = 'POST, GET, OPTIONS'
    headers['Access-Control-Allow-Headers'] = '*'
    headers['Access-Control-Max-Age'] = "1728000"
  end

  # If this is a preflight OPTIONS request, then short-circuit the
  # request, return only the necessary headers and return an empty
  # text/plain.

  def cors_preflight_check
    if request.method == :options
      headers['Access-Control-Allow-Origin'] = '*'
      headers['Access-Control-Allow-Methods'] = 'POST, PUT, DELETE, GET, OPTIONS'
      headers['Access-Control-Request-Method'] = '*'
      headers['Access-Control-Allow-Headers'] = 'Origin, X-Requested-With, Content-Type, Accept, Authorization'
    end
  end

  def after_sign_in_path_for(resource)
    if current_user.player
      player_path(current_user.player)
    elsif current_user.delegate
      delegate_path(current_user.delegate)
    elsif current_user.admin
      admins_path
    else
      redirect_to root_path
    end
  end

  def if_player
    # (current_user.player || current_user.delegate || current_user.admin)
    redirect_to root_path flash[:error] = "You don't have permissions! Login please!" unless logged_in?
  end

  def if_deleagate
    current_user.delegate || current_user.admin
  end

  def if_administrator
    if logged_in?
      current_user.admin
    else
      redirect_to root_path flash[:error] = "You don't have permissions! Login please!" unless logged_in?
    end
  end

  #def current_user
    # || logicko "ili", odnosno "+"
    # ||= je veoma slicno operatoru +=
  #  @current_user ||= User.find(session[:user_id]) if session[:user_id]
  #end

  # Cuvamo u memoriji browsera koji je mec u toku (potrebno za delegate)
  def current_match
    @current_match ||= Match.find(session[:match_id]) if session[:match_id]
  end

  def is_match_started
    if !current_match.is_match_started
      flash[:danger] = "Morate da startujete meč!"
      redirect_to match_path(current_match)
    end
  end

  def is_home_team_players_chosen
    if !current_match.is_home_team_players_chosen
      flash[:danger] = "Morate da izaberete igrače za meč!"
      redirect_to match_path(current_match)
    end
  end

  def is_away_team_players_chosen
    if !current_match.is_away_team_players_chosen
      flash[:danger] = "Morate da izaberete igrače za meč!"
      redirect_to match_path(current_match)
    end
  end

  # Nesto sam testirao, ne treba
  #def current_home
  #  @current_home ||= session[:home_id]
  #end

  #def current_away
  #  @current_away ||= session[:away_id]
  #end
  
  # Kada imamo upitnik iza promenljive, onda je promenljiva boolean
  def logged_in?
    # "!" predstavlja negaciju
    # Dve negacije imamo da bi dobili 0 ili 1 (boolean)
    !!current_user
  end
  
  def require_user
    # Ovo citamo: "ako nije ulogovan"
    if !logged_in?
      flash[:danger] = "You must be logged in to perform that action"
      redirect_to root_path
    end
  end

  # Ovaj metod nam sluzi za navbar
  def leagues_nav
    @lige4plus1_nav = League.where(:is_fourplusone => "1")
    @lige5plus1_nav = League.where(:is_fiveplusone => "1")
  end

  # Ovaj metod nam sluzi za navbar
  def tournaments_nav
    @tournaments4plus1_nav = Tournament.where(:is_fourplusone => "1")
    @tournaments5plus1_nav = Tournament.where(:is_fiveplusone => "1")
  end

  ############################################
  # Ova komanda je za sistem za razmenu poruka 
  ############################################

  def redirect_back_or(path)
    redirect_to request.referer || path
  end

  def organisation_info
    @organisation_info = OrganisationInformation.second
  end

  def language
    #if @organisation_info.is_english
      #@organisation_info.update(:is_serbian => false)
      @language = Language.second
      @language_2 = L2anguage.second
    #elsif @organisation_info.is_english

      @playerbadges_language = PlayerbadgesLanguage.second
      @teambadges_language = TeambadgesLanguage.second
  end

  ##########################################
  # PROTECTED
  ##########################################
  protected

  # Potrebno za autentifikaciju putem JSON-a
  #def configure_permitted_parameters
  #  devise_parameter_sanitizer.for(:sign_up).push(:username, :email)
  # devise_parameter_sanitizer.for(:account_update).push(:username, :email)
  #end

  #########################################################
  # :username jer u tabeli users imamo username kao atribut
  #########################################################

  #def configure_permitted_parameters
  #  devise_parameter_sanitizer.for(:sign_up) << :username
  #  devise_parameter_sanitizer.for(:account_update) << :username
  #end

  ###############################################
  
end
