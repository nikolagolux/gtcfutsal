class SeasonsController < ApplicationController
  before_action :set_season, only:[:edit, :update, :show]

  def index
    @seasons = Season.all
  end

  def new
    @season = Season.new
  end

  def create
   @season = Season.new(season_params)
    if @season.save
      flash[:success] = "Welcome #{@season.name}"
      redirect_to seasons_path
    else
      render 'new'
    end
  end

  def edit
  end

  def update
  end

  def show
  end

  private
  def set_season
    @season = Season.find(params[:id])
  end

  def season_params
    params.require(:season).permit(:name, :registration_token)
  end
end
