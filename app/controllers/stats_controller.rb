class StatsController < ApplicationController
  
  def index
  end

  # Tabela PLAYERS
  def show

    # Isprobano i radi!
    @league = League.find(params[:id])
    @teams = Team.where(:league_id => @league.id)
    if @teams != nil
      i = 0
      @teams.each do |team|
        @players_per_one_team = Player.where(:team_id => team.id)
        if i == 0 then
          @players = @players_per_one_team
        else 
          @players.concat(@players_per_one_team)
        end
        i = 1
      end
    end
    # sada se u @players nalaze svi igraci iz odredjene lige
    if @players != nil
      i = 0
      @players.each do |player|
        @player_season = PlayerSeason.where(:player_id => player.id)
        if i == 0 then
          @player_seasons = @player_season
        else 
          @player_seasons.concat(@player_season)
        end
        i = 1
      end
      # sada imamo sve @player_seasons

      # Logika za sortiranje igraca
      pozicija = 1
      @player_seasons.sort_by{|e| -e[:goals]}.each do |player_season|
        player_season.update(:position => pozicija)
        pozicija += 1
      end
    end
  end
end
