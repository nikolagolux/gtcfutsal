class TeamPlayersController < ApplicationController
  before_action :set_team, only:[:edit, :update, :show]

  def index
  end

  def show
  	@players = Player.where(:team_id => @team.id)

    if @players != nil
      j = 0
      @players.each do |player|
          @player_season = PlayerSeason.where(:player_id => player.id, :season_id => Season.last.id)
          if j == 0 then
            @player_seasons = @player_season
          else 
            @player_seasons.concat(@player_season)
          end
          j = 1
      end
    end

    @team_season = TeamSeason.find_by(:team_id => @team.id, :season_id => Season.last.id)

    @ime_tima = @team.name
    @broj_pobeda_tima = @team_season.matches_win
    @broj_neresenih_tima = @team_season.matches_draw
    @broj_izgubljenih_tima = @team_season.matches_lose
    @broj_odigranih_meceva = @broj_pobeda_tima + @broj_neresenih_tima + @broj_izgubljenih_tima
    # matches_played se racuna ovde
    @team_season.update(:matches_played => @broj_odigranih_meceva)
    @procenat_pobeda = @broj_pobeda_tima.to_f * 100 / @broj_odigranih_meceva
    @procenat_neresenih = @broj_neresenih_tima.to_f * 100 / @broj_odigranih_meceva
    @procenat_izgubljenih = @broj_izgubljenih_tima.to_f * 100 / @broj_odigranih_meceva 

    @level = @team_season[:level]
    # Izracunavanje koliko procenata od iduceg levela je osvojio igrac
    if @level == 1
      @procenata_za_iduci_level = (100*@team_season[:expirience])/250
      @procenata_za_iduci_level = @procenata_za_iduci_level.round
    elsif @level == 2
      @procenata_za_iduci_level = (100*(@team_season[:expirience]-250)/1250)
      @procenata_za_iduci_level = @procenata_za_iduci_level.round
    elsif @level == 3
      @procenata_za_iduci_level = (100*(@team_season[:expirience]-1500)/2000)
      @procenata_za_iduci_level = @procenata_za_iduci_level.round
    elsif @level == 4
      @procenata_za_iduci_level = (100*(@team_season[:expirience]-3500)/2500)
      @procenata_za_iduci_level = @procenata_za_iduci_level.round
    elsif @level == 5
      @procenata_za_iduci_level = (100*(@team_season[:expirience]-6000)/4000)
      @procenata_za_iduci_level = @procenata_za_iduci_level.round
    elsif @level == 6
      @procenata_za_iduci_level = (100*(@team_season[:expirience]-10000)/4000)
      @procenata_za_iduci_level = @procenata_za_iduci_level.round
    elsif @level == 7
      @procenata_za_iduci_level = (100*(@team_season[:expirience]-14000)/4000)
      @procenata_za_iduci_level = @procenata_za_iduci_level.round
    elsif @level == 8
      @procenata_za_iduci_level = (100*(@team_season[:expirience]-18000)/4000)
      @procenata_za_iduci_level = @procenata_za_iduci_level.round
    elsif @level == 9
      @procenata_za_iduci_level = (100*(@team_season[:expirience]-22000)/4000)
      @procenata_za_iduci_level = @procenata_za_iduci_level.round
    elsif @level == 10
      @procenata_za_iduci_level = (100*(@team_season[:expirience]-26000)/4000)
      @procenata_za_iduci_level = @procenata_za_iduci_level.round
    elsif @level == 11
      @procenata_za_iduci_level = (100*(@team_season[:expirience]-30000)/4000)
      @procenata_za_iduci_level = @procenata_za_iduci_level.round
    elsif @level == 11
      @procenata_za_iduci_level = (100*(@team_season[:expirience]-34000)/4000)
      @procenata_za_iduci_level = @procenata_za_iduci_level.round
    elsif @level == 12
      @procenata_za_iduci_level = (100*(@team_season[:expirience]-38000)/4000)
      @procenata_za_iduci_level = @procenata_za_iduci_level.round
    elsif @level == 13
      @procenata_za_iduci_level = (100*(@team_season[:expirience]-42000)/4000)
      @procenata_za_iduci_level = @procenata_za_iduci_level.round
    elsif @level == 14
      @procenata_za_iduci_level = (100*(@team_season[:expirience]-46000)/4000)
      @procenata_za_iduci_level = @procenata_za_iduci_level.round
    elsif @level == 15
      @procenata_za_iduci_level = (100*(@team_season[:expirience]-50000)/4000)
      @procenata_za_iduci_level = @procenata_za_iduci_level.round
    elsif @level == 16
      @procenata_za_iduci_level = (100*(@team_season[:expirience]-54000)/4000)
      @procenata_za_iduci_level = @procenata_za_iduci_level.round
    elsif @level == 17
      @procenata_za_iduci_level = (100*(@team_season[:expirience]-58000)/4000)
      @procenata_za_iduci_level = @procenata_za_iduci_level.round
    elsif @level == 18
      @procenata_za_iduci_level = (100*(@team_season[:expirience]-62000)/4000)
      @procenata_za_iduci_level = @procenata_za_iduci_level.round
    elsif @level == 19
      @procenata_za_iduci_level = (100*(@team_season[:expirience]-66000)/4000)
      @procenata_za_iduci_level = @procenata_za_iduci_level.round
    elsif @level == 20
      @procenata_za_iduci_level = (100*(@team_season[:expirience]-70000)/4000)
      @procenata_za_iduci_level = @procenata_za_iduci_level.round
    elsif @level == 21
      @procenata_za_iduci_level = (100*(@team_season[:expirience]-74000)/4000)
      @procenata_za_iduci_level = @procenata_za_iduci_level.round
    elsif @level == 22
      @procenata_za_iduci_level = (100*(@team_season[:expirience]-78000)/4000)
      @procenata_za_iduci_level = @procenata_za_iduci_level.round
    elsif @level == 23
      @procenata_za_iduci_level = (100*(@team_season[:expirience]-82000)/4000)
      @procenata_za_iduci_level = @procenata_za_iduci_level.round
    elsif @level == 24
      @procenata_za_iduci_level = (100*(@team_season[:expirience]-86000)/4000)
      @procenata_za_iduci_level = @procenata_za_iduci_level.round
    elsif @level == 25
      @procenata_za_iduci_level = (100*(@team_players.team.team_season[:expirience]-90000)/4000)
      @procenata_za_iduci_level = @procenata_za_iduci_level.round
    end
  end

  def update
  end

  private
  def set_team
    @team = Team.find(params[:id])
  end

end
