class TournamentsController < ApplicationController
  before_action :set_tournament, only:[:edit, :update, :show]


  def index
  end

  def show
      @matches = Match.where(:tournament_id => @tournament.id, :is_qualification_match => 1, :is_match_started => 1, :is_match_finished => 1)
    	# Nalazimo mec na prvoj poziciji
    	@match_1 = Match.find_by(:tournament_id => @tournament.id, :tournament_position => 1)
    	unless @match_1 == nil
        @home_team_1 = @match_1.home_team.team
    	  @away_team_1 = @match_1.away_team.team
      end

    	@match_2 = Match.find_by(:tournament_id => @tournament.id, :tournament_position => 2)
    	unless @match_2 == nil
        @home_team_2 = @match_2.home_team.team
      	@away_team_2 = @match_2.away_team.team
      end

    	@match_3 = Match.find_by(:tournament_id => @tournament.id, :tournament_position => 3)
    	unless @match_3 == nil
        @home_team_3 = @match_3.home_team.team
      	@away_team_3 = @match_3.away_team.team
      end

    	@match_4 = Match.find_by(:tournament_id => @tournament.id, :tournament_position => 4)
    	unless @match_4 == nil
        @home_team_4 = @match_4.home_team.team
      	@away_team_4 = @match_4.away_team.team
      end
    	@match_5 = Match.find_by(:tournament_id => @tournament.id, :tournament_position => 5)
    	unless @match_5 == nil
        @home_team_5 = @match_5.home_team.team
      	@away_team_5 = @match_5.away_team.team
      end
    	@match_6 = Match.find_by(:tournament_id => @tournament.id, :tournament_position => 6)
    	unless @match_6 == nil
        @home_team_6 = @match_6.home_team.team
      	@away_team_6 = @match_6.away_team.team
      end
    	@match_7 = Match.find_by(:tournament_id => @tournament.id, :tournament_position => 7)
    	unless @match_7 == nil
        @home_team_7 = @match_7.home_team.team
      	@away_team_7 = @match_7.away_team.team
      end
    	@match_8 = Match.find_by(:tournament_id => @tournament.id, :tournament_position => 8)
    	unless @match_8 == nil
        @home_team_8 = @match_8.home_team.team
      	@away_team_8 = @match_8.away_team.team
      end

      unless @match_1 == nil || @match_2 == nil
      	if @match_1.is_match_finished && @match_2.is_match_finished 
      	   	# Nalazimo mec na devetoj poziciji
      	  	@match_9 = Match.find_by(:tournament_id => @tournament.id, :tournament_position => 9)
      	  	@home_team_9 = @match_9.home_team.team
      	  	@away_team_9 = @match_9.away_team.team
      	end
      end

    unless @match_3 == nil || @match_4 == nil
      if @match_3.is_match_finished && @match_4.is_match_finished 
          # Nalazimo mec na desetoj poziciji
          @match_10 = Match.find_by(:tournament_id => @tournament.id, :tournament_position => 10)
          @home_team_10 = @match_10.home_team.team
          @away_team_10 = @match_10.away_team.team
      end
    end

    unless @match_5 == nil || @match_6 == nil
      if @match_5.is_match_finished && @match_6.is_match_finished 
          # Nalazimo mec na jedanestoj poziciji
          @match_11 = Match.find_by(:tournament_id => @tournament.id, :tournament_position => 11)
          @home_team_11 = @match_11.home_team.team
          @away_team_11 = @match_11.away_team.team
      end
    end

    unless @match_7 == nil || @match_8 == nil
      if @match_7.is_match_finished && @match_8.is_match_finished 
          # Nalazimo mec na dvanestoj poziciji
          @match_12 = Match.find_by(:tournament_id => @tournament.id, :tournament_position => 12)
          @home_team_12 = @match_12.home_team.team
          @away_team_12 = @match_12.away_team.team
      end
    end


    unless @match_1 == nil || @match_2 == nil || @match_3 == nil || @match_4 == nil || @match_5 == nil || @match_6 == nil || @match_7 == nil || @match_8 == nil
      if @match_1.is_match_finished && @match_2.is_match_finished && @match_3.is_match_finished &&  @match_4.is_match_finished && 
              @match_5.is_match_finished && @match_6.is_match_finished && @match_7.is_match_finished && @match_8.is_match_finished
        if @match_9.is_match_finished && @match_10.is_match_finished 
            # Nalazimo mec na trinestoj poziciji
            @match_13 = Match.find_by(:tournament_id => @tournament.id, :tournament_position => 13)
            @home_team_13 = @match_13.home_team.team
            @away_team_13 = @match_13.away_team.team
        end
      end
      if @match_1.is_match_finished && @match_2.is_match_finished && @match_3.is_match_finished &&  @match_4.is_match_finished && 
                @match_5.is_match_finished && @match_6.is_match_finished && @match_7.is_match_finished && @match_8.is_match_finished
          if @match_11.is_match_finished && @match_12.is_match_finished 
              # Nalazimo mec na trinestoj poziciji
              @match_14 = Match.find_by(:tournament_id => @tournament.id, :tournament_position => 14)
              @home_team_14 = @match_14.home_team.team
              @away_team_14 = @match_14.away_team.team
          end
      end
      if @match_1.is_match_finished && @match_2.is_match_finished && @match_3.is_match_finished &&  @match_4.is_match_finished && 
            @match_5.is_match_finished && @match_6.is_match_finished && @match_7.is_match_finished && @match_8.is_match_finished
        if @match_9.is_match_finished && @match_10.is_match_finished && @match_11.is_match_finished && @match_12.is_match_finished
          if @match_13.is_match_finished && @match_14.is_match_finished 
              # Nalazimo mec na finalnoj poziciji
              @match_15 = Match.find_by(:tournament_id => @tournament.id, :tournament_position => 15)
              @home_team_15 = @match_15.home_team.team
              @away_team_15 = @match_15.away_team.team
          end
        end
      end
    end

  end

  private
  def set_tournament
    @tournament = Tournament.find(params[:id])
  end
end
