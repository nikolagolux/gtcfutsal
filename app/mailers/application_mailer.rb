class ApplicationMailer < ActionMailer::Base
  default from: "balf.kontakt@gmail.com"
  layout 'mailer'
end
