class Article < ActiveRecord::Base
  belongs_to :user
  belongs_to :league
  has_attached_file :avatar, styles: { medium: "377.33x250>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/
  validates :title, presence: true, length: { minimum: 3, maximum: 50 }
  validates :description, presence: true, length: { minimum: 10, maximum: 3000 }
  #validates :user_id, presence: true

  def author_name
  	if self.user.player
  		"#{self.user.player.name} #{self.user.player.surname}"
  	elsif self.user.delegate
		"#{self.user.delegate.name} #{self.user.delegate.surname}"
  	elsif self.user.admin
  		"#{self.user.admin.name} #{self.user.admin.surname}"
  	end
  end
end