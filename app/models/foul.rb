class Foul < ActiveRecord::Base
	after_create :increment_player_season_foul,
				 :increment_current_match_foul

	belongs_to :match
	belongs_to :player_season

	private
	def increment_player_season_foul
		#PlayerSeason.first.update(:fouls => 7)
		PlayerSeason.increment_counter(:fouls, self.player_season)
	end

	def increment_current_match_foul
		#PlayerSeason.first.update(:fouls => 7)
		if self.is_home
			Match.increment_counter(:home_fouls, self.match)
		else
			Match.increment_counter(:away_fouls, self.match)
		end
	end
end 