class Match < ActiveRecord::Base
	
	has_many :goals
	has_many :assists
	has_many :fouls
	has_many :yellow_cards
	has_many :red_cards
	has_many :shoots
	belongs_to :referee
	belongs_to :delegate
	belongs_to :stadion
	belongs_to :home_team
	belongs_to :away_team
	belongs_to :league
	belongs_to :tournament
    belongs_to :season

    def start_date
        "Datum: #{self.match_date.strftime("%d.%m.%Y")}"
    end

    def start_time
        "Vreme: #{self.match_date.strftime("%H:%M")}"
    end

    ######################################
    # HOME TEAM
    ######################################
    def home_team_name
        if self.home_team !=nil
            self.home_team.team.name
        end
    end

    def home_team_avatar
        if self.home_team !=nil
            "http://balf.rs#{self.home_team.team.avatar.url(:original)}"
        end
    end

    ######################################
    # AWAY TEAM
    ######################################
    def away_team_name
        if self.away_team !=nil
            self.away_team.team.name
        end
    end

    def away_team_avatar
        if self.away_team !=nil
        "http://balf.rs#{self.away_team.team.avatar.url(:original)}"
        end
    end

    #def get_all_home_teams
    #    HomeTeam.all.map{|x| x.name}
    #end

    #rails_admin do
    #    edit do
    #      configure :home_team_id do
    #        searchable true
    #        queryable true
    #      end
          # field :room do
          #   searchable room: :name
          # end
    #    end
    #end
end
