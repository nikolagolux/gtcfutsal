class MatchPlayer < ActiveRecord::Base
  #############################################################
  # Ne smemo da imamo vise player sezona sa istom kombinacijom
  # (player_id, season_id)
  #############################################################
  validates_uniqueness_of :player_season_id, :scope => :match_id
  #############################################################

  def player_avatar_url
  	player_season = PlayerSeason.find_by(:id => self.player_season_id)
    "http://balf.rs#{player_season.player.avatar.url(:original)}"
  end

  def player_name
  	PlayerSeason.find_by(:id => self.player_season_id).player.name
  end

  def player_surname
  	PlayerSeason.find_by(:id => self.player_season_id).player.surname
  end

  def player_id
  	PlayerSeason.find_by(:id => self.player_season_id).player.id
  end

  def player_goals
  	Goal.where(:player_season_id => self.player_season_id, :match_id => self.match_id).count
  end

  def player_assists
  	Assist.where(:player_season_id => self.player_season_id, :match_id => self.match_id).count
  end

  def player_y_c
  	YellowCard.where(:player_season_id => self.player_season_id, :match_id => self.match_id).count
  end

  def player_r_c
  	RedCard.where(:player_season_id => self.player_season_id, :match_id => self.match_id).count
  end

  def player_gk_saves
  	GoalkeeperSave.where(:player_season_id => self.player_season_id, :match_id => self.match_id).count
  end
end
