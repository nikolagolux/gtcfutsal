class OrganisationInformation < ActiveRecord::Base
  has_attached_file :avatar, :styles => { :medium => "300x300>", :thumb => "100x100>", :mapa => "32x32>" }, :default_url => lambda { |avatar| avatar.instance.set_default_url}
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/

  def set_default_url
    ActionController::Base.helpers.asset_path('missing.png')
  end
end