class Player < ActiveRecord::Base
  after_create :create_new_player_seasons,
               :create_new_player_badges

  before_destroy :destroy_player_season,
                 :destroy_player_badge

  has_one :user
  belongs_to :team
  has_many :player_seasons
  has_many :player_badges

  accepts_nested_attributes_for :user#, :team

  has_attached_file :avatar, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => lambda { |avatar| avatar.instance.set_default_url}
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/
  validates :JMBG, presence:true

  def set_default_url
    ActionController::Base.helpers.asset_path('missing.png')
  end

  # Attributes for RAILS REST API
  def avatar_url
    "http://balf.rs#{self.avatar.url(:original)}"
  end

  def name_with_initial
  	"#{id} #{name} #{surname}"
  end

  def level
    "#{self.player_season.level}"
  end

  def experience
    "#{self.player_season.expirience}"
  end

  def full_name
    "#{name} #{surname}"
  end

  def full_name_and_team
    "#{name} #{surname}, #{self.team.name}"
  end

  def league_id
    if self.team != nil
      if self.team.league != nil
        self.team.league.id
      end
    end
  end

	private
  ########################################################
  # AFTER CREATE PLAYER
  ########################################################
	def create_new_player_seasons
		#Player.update(:season_id)
		@players=Player.where(:team_id => self.team_id)
		@players.each do |player|
			@season = Season.last
			PlayerSeason.create(:season_id => @season.id ,:player_id => player.id)
		end
	end

  # Metoda koja kreira sve bedzeve za jednog igraca
  def create_new_player_badges
    #Player.update(:season_id)
    @players=Player.where(:team_id => self.team_id)
    @players.each do |player|
      @season = Season.last
      PlayerBadge.create(:player_id => player.id, :season_id => @season.id)
    end
  end

  ########################################################
  # BEFORE DESTROY PLAYER
  ########################################################
  def destroy_player_season
    #Player.update(:season_id)
    PlayerSeason.where(:player_id => self.id).destroy_all
  end

  # Metoda koja kreira sve bedzeve za jednog igraca
  def destroy_player_badge
    #Player.update(:season_id)
    PlayerBadge.where(:player_id => self.id).destroy_all
  end

end