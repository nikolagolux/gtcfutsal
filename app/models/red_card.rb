class RedCard < ActiveRecord::Base
	after_create :increment_player_season_red_card,
				 :increment_current_match_red_card,
				 :increment_team_season_total_red_card

	belongs_to :match
	belongs_to :player_season

	private
	def increment_player_season_red_card
		#PlayerSeason.first.update(:red_cards => 7)
		PlayerSeason.increment_counter(:red_cards, self.player_season)
	end

	def increment_current_match_red_card
		#PlayerSeason.first.update(:red_cards => 7)
		if self.is_home
			Match.increment_counter(:home_red_cards, self.match)
		else
			Match.increment_counter(:away_red_cards, self.match)
		end
	end

	def increment_team_season_total_red_card
		@total_red_cards = TeamSeason.find_by(:team_id => self.player_season.player.team.id, :season_id => Season.last.id)
		TeamSeason.increment_counter(:total_red_cards, @total_red_cards)
	end
end