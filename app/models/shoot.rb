class Shoot < ActiveRecord::Base
	after_create :increment_team_season_shoot,
				 :increment_current_match_shoot

	belongs_to :match
	belongs_to :team_season

	private
	def increment_team_season_shoot
		#PlayerSeason.first.update(:fouls => 7)
		@shoot = TeamSeason.find_by(:team_id => self.team_season.team.id, :season_id => Season.last.id)
		TeamSeason.increment_counter(:shoots, @shoot)
	end

	def increment_current_match_shoot
		#PlayerSeason.first.update(:fouls => 7)
		#id_prethodnog_suta = self.id - 1
		#prethodni_sut = Shoot.find_by(:id => id_prethodnog_suta)
		#unless self.created_at.seconds_since_midnight < (prethodni_sut.created_at.seconds_since_midnight + 3)
			if self.is_home
				Match.increment_counter(:home_shoots, self.match)
			else
				Match.increment_counter(:away_shoots, self.match)
			end
		#end
	end
end 