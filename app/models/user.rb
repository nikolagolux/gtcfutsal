class User < ActiveRecord::Base
  belongs_to :player
  belongs_to :delegate
  belongs_to :admin
  has_many  :articles

  # Trebalo bi da dodamo :confirmable
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

         
  ############################################
  # Ova komanda je za sistem za razmenu poruka 
  ############################################
  acts_as_messageable

  ############################################
  # Ova komanda je za sistem za postavljanje
  # komentara
  ############################################
  acts_as_commontator

# Potrebno za autentifikaciju na mobilnom telefonu
  def fetch_auth_token!
    self.auth_token = SecureRandom.base64(50)
    self.save!
    self.auth_token
  end

  def mailboxer_email(object)
    email
  end
end
