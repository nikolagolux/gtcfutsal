class MatchSerializer < ActiveModel::Serializer
  attributes :id, :home_team_name, :away_team_name, :home_team_avatar, :start_time, :start_date,
  			 :away_team_avatar, :home_goals, :away_goals, :delegate_id, :is_match_finished,
  			 :home_team_id, :away_team_id, :home_assists, :away_assists, :home_fouls, :away_fouls,
  			 :home_yellow_cards, :away_yellow_cards, :home_red_cards, :away_red_cards,
  			 :home_goalkeeper_saves, :away_goalkeeper_saves, :home_shoots, :away_shoots, :league_id
end
