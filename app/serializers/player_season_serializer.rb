class PlayerSeasonSerializer < ActiveModel::Serializer
  attributes :id, :position, :name, :surname, :full_name, :team_name,
  			 :goals, :assists, :fouls, :yellow_cards, :red_cards, :matches_played,
  			 :level, :expirience, :matches_played, :matches_win, :matches_draw,
  			 :matches_lose, :goalkeeper_saves, :player_id
end
