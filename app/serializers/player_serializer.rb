class PlayerSerializer < ActiveModel::Serializer
  attributes :id, :name, :surname, :avatar_url, :level, :experience, :team_id, :league_id
end
