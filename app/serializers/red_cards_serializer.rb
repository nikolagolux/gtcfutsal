class RedCardSerializer < ActiveModel::Serializer
  attributes :id, :player_season_id, :match_id, :is_home
end
