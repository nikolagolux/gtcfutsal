class ShootSerializer < ActiveModel::Serializer
  attributes :id, :match_id, :team_season_id, :is_home
end
