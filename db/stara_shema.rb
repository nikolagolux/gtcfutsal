# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161101184943) do

  create_table "admins", force: :cascade do |t|
    t.string "name",    limit: 50
    t.string "surname", limit: 70
  end

  create_table "articles", force: :cascade do |t|
    t.string   "title",               limit: 80,                       null: false
    t.text     "description",         limit: 16777215,                 null: false
    t.integer  "user_id",             limit: 8
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "avatar_file_name",    limit: 255
    t.string   "avatar_content_type", limit: 255
    t.integer  "avatar_file_size",    limit: 8
    t.datetime "avatar_updated_at"
    t.boolean  "is_league_article",                    default: false
    t.boolean  "is_general_article",                   default: false
    t.integer  "league_id",           limit: 8
  end

  add_index "articles", ["league_id"], name: "league_id", using: :btree
  add_index "articles", ["user_id"], name: "user_id", using: :btree

  create_table "assists", force: :cascade do |t|
    t.integer  "player_season_id", limit: 8,                 null: false
    t.integer  "match_id",         limit: 8,                 null: false
    t.boolean  "is_home",                    default: false
    t.integer  "created_at",       limit: 8
    t.datetime "updated_at"
  end

  add_index "assists", ["match_id"], name: "match_id", using: :btree
  add_index "assists", ["player_season_id"], name: "player_season_id", using: :btree

  create_table "away_teams", force: :cascade do |t|
    t.integer "team_id", limit: 8, null: false
  end

  add_index "away_teams", ["team_id"], name: "team_id", using: :btree

  create_table "commontator_comments", force: :cascade do |t|
    t.string   "creator_type",      limit: 255
    t.integer  "creator_id",        limit: 4
    t.string   "editor_type",       limit: 255
    t.integer  "editor_id",         limit: 4
    t.integer  "thread_id",         limit: 4,                 null: false
    t.text     "body",              limit: 65535,             null: false
    t.datetime "deleted_at"
    t.integer  "cached_votes_up",   limit: 4,     default: 0
    t.integer  "cached_votes_down", limit: 4,     default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "commontator_comments", ["cached_votes_down"], name: "index_commontator_comments_on_cached_votes_down", using: :btree
  add_index "commontator_comments", ["cached_votes_up"], name: "index_commontator_comments_on_cached_votes_up", using: :btree
  add_index "commontator_comments", ["creator_id", "creator_type", "thread_id"], name: "index_commontator_comments_on_c_id_and_c_type_and_t_id", using: :btree
  add_index "commontator_comments", ["thread_id", "created_at"], name: "index_commontator_comments_on_thread_id_and_created_at", using: :btree

  create_table "commontator_subscriptions", force: :cascade do |t|
    t.string   "subscriber_type", limit: 255, null: false
    t.integer  "subscriber_id",   limit: 4,   null: false
    t.integer  "thread_id",       limit: 4,   null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "commontator_subscriptions", ["subscriber_id", "subscriber_type", "thread_id"], name: "index_commontator_subscriptions_on_s_id_and_s_type_and_t_id", unique: true, using: :btree
  add_index "commontator_subscriptions", ["thread_id"], name: "index_commontator_subscriptions_on_thread_id", using: :btree

  create_table "commontator_threads", force: :cascade do |t|
    t.string   "commontable_type", limit: 255
    t.integer  "commontable_id",   limit: 4
    t.datetime "closed_at"
    t.string   "closer_type",      limit: 255
    t.integer  "closer_id",        limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "commontator_threads", ["commontable_id", "commontable_type"], name: "index_commontator_threads_on_c_id_and_c_type", unique: true, using: :btree

  create_table "delegates", force: :cascade do |t|
    t.string   "name",                        limit: 50
    t.string   "surname",                     limit: 50
    t.string   "JMBG",                        limit: 20
    t.integer  "delegate_registration_token", limit: 8
    t.boolean  "is_approved"
    t.string   "avatar_file_name",            limit: 255
    t.string   "avatar_content_type",         limit: 255
    t.integer  "avatar_file_size",            limit: 8
    t.datetime "avatar_updated_at"
  end

  create_table "fouls", force: :cascade do |t|
    t.integer  "player_season_id", limit: 8,                 null: false
    t.integer  "match_id",         limit: 8,                 null: false
    t.boolean  "is_home",                    default: false
    t.integer  "created_at",       limit: 8
    t.datetime "updated_at"
  end

  add_index "fouls", ["match_id"], name: "match_id", using: :btree
  add_index "fouls", ["player_season_id"], name: "player_season_id", using: :btree

  create_table "goalkeeper_saves", force: :cascade do |t|
    t.integer  "player_season_id", limit: 8,                 null: false
    t.integer  "match_id",         limit: 8,                 null: false
    t.boolean  "is_home",                    default: false
    t.integer  "created_at",       limit: 8
    t.datetime "updated_at"
  end

  add_index "goalkeeper_saves", ["match_id"], name: "match_id", using: :btree
  add_index "goalkeeper_saves", ["player_season_id"], name: "player_season_id", using: :btree

  create_table "goals", force: :cascade do |t|
    t.integer  "player_season_id",  limit: 8,                 null: false
    t.integer  "match_id",          limit: 8,                 null: false
    t.boolean  "is_home",                     default: false
    t.boolean  "iz_slobodnog",                default: false
    t.boolean  "iz_kornera",                  default: false
    t.boolean  "glavom",                      default: false
    t.boolean  "sa_svoje_polovine",           default: false
    t.integer  "created_at",        limit: 8
    t.datetime "updated_at"
  end

  add_index "goals", ["match_id"], name: "match_id", using: :btree
  add_index "goals", ["player_season_id"], name: "player_season_id", using: :btree

  create_table "home_teams", force: :cascade do |t|
    t.integer "team_id", limit: 8, null: false
  end

  add_index "home_teams", ["team_id"], name: "team_id", using: :btree

  create_table "languages", force: :cascade do |t|
    t.string   "unesite_email",                 limit: 60
    t.string   "unesite_sifru",                 limit: 60
    t.string   "brojac_igraca",                 limit: 60
    t.string   "brojac_timova",                 limit: 60
    t.string   "brojac_balona",                 limit: 60
    t.string   "brojac_liga",                   limit: 60
    t.string   "meni_vesti",                    limit: 60
    t.string   "meni_info",                     limit: 60
    t.string   "meni_kup",                      limit: 60
    t.string   "meni_pravila",                  limit: 60
    t.string   "meni_registruj_ekipu",          limit: 60
    t.string   "meni_kontakt",                  limit: 60
    t.string   "meni_tereni",                   limit: 60
    t.string   "meni_sudije",                   limit: 60
    t.string   "meni_tabele",                   limit: 60
    t.string   "meni_statistike",               limit: 60
    t.string   "meni_rezultati",                limit: 60
    t.string   "meni_uzivo_utakmice",           limit: 60
    t.string   "meni_stranice_liga",            limit: 60
    t.string   "meni_rang_lista_ekipa",         limit: 60
    t.string   "meni_rang_lista_igraca",        limit: 60
    t.string   "meni_kontakt",                  limit: 60
    t.string   "meni_kup_5_plus_1",             limit: 60
    t.string   "meni_kup_4_plus_1",             limit: 60
    t.string   "meni_pravilnik_5_plus_1",       limit: 60
    t.string   "meni_pravilnik_4_plus_1",       limit: 60
    t.string   "footer_prijavi_se",             limit: 60
    t.string   "footer_novi_delegat",           limit: 60
    t.string   "pozicija",                      limit: 60
    t.string   "pozicija_skraceno",             limit: 60
    t.string   "ekipa",                         limit: 60
    t.string   "ekipa_skraceno",                limit: 60
    t.string   "igrac",                         limit: 60
    t.string   "igrac_skraceno",                limit: 60
    t.string   "golova",                        limit: 60
    t.string   "golova_skraceno",               limit: 60
    t.string   "asistencija",                   limit: 60
    t.string   "asistencija_skraceno",          limit: 60
    t.string   "zutih_kartona",                 limit: 60
    t.string   "zutih_kartona_skraceno",        limit: 60
    t.string   "crvenih_kartona",               limit: 60
    t.string   "crvenih_kartona_skraceno",      limit: 60
    t.string   "nastupa",                       limit: 60
    t.string   "nastupa_skraceno",              limit: 60
    t.string   "pobeda",                        limit: 60
    t.string   "pobeda_skraceno",               limit: 60
    t.string   "neresenih",                     limit: 60
    t.string   "neresenih_skraceno",            limit: 60
    t.string   "izgubljenih",                   limit: 60
    t.string   "izgubljenih_skraceno",          limit: 60
    t.string   "dato_golova",                   limit: 60
    t.string   "dato_golova_skraceno",          limit: 60
    t.string   "primljeno_golova",              limit: 60
    t.string   "primljeno_golova_skraceno",     limit: 60
    t.string   "gol_razlika",                   limit: 60
    t.string   "gol_razlika_skraceno",          limit: 60
    t.string   "bodova",                        limit: 60
    t.string   "bodova_skraceno",               limit: 60
    t.string   "datum",                         limit: 60
    t.string   "vreme",                         limit: 60
    t.string   "domacin",                       limit: 60
    t.string   "domacin_skraceno",              limit: 60
    t.string   "rezultat",                      limit: 60
    t.string   "rezultat_skraceno",             limit: 60
    t.string   "gost",                          limit: 60
    t.string   "gost_skraceno",                 limit: 60
    t.string   "liga",                          limit: 60
    t.string   "liga_skraceno",                 limit: 60
    t.string   "sezona",                        limit: 60
    t.string   "sezona_skraceno",               limit: 60
    t.string   "teren",                         limit: 60
    t.string   "teren_skraceno",                limit: 60
    t.string   "podesavanja_naloga",            limit: 60
    t.string   "share",                         limit: 60
    t.string   "toggle_navigation",             limit: 60
    t.string   "your_conversation",             limit: 60
    t.string   "posalji",                       limit: 60
    t.string   "pratite_nas",                   limit: 60
    t.string   "godina",                        limit: 60
    t.string   "potvrdi",                       limit: 60
    t.string   "azuriraj_nalog",                limit: 60
    t.string   "prepiska_sa",                   limit: 60
    t.string   "restore",                       limit: 60
    t.string   "poruka",                        limit: 60
    t.string   "are_you_sure",                  limit: 60
    t.string   "primljene_poruke",              limit: 60
    t.string   "poslate_poruke",                limit: 60
    t.string   "kanta",                         limit: 60
    t.string   "isprazni_kantu",                limit: 60
    t.string   "informacije_o_meni",            limit: 60
    t.string   "moj_profil",                    limit: 60
    t.string   "kalendar_takmicenja",           limit: 60
    t.string   "my_articles",                   limit: 60
    t.string   "log_out",                       limit: 60
    t.string   "created",                       limit: 60
    t.string   "create_article",                limit: 60
    t.string   "edit_this_article",             limit: 60
    t.string   "delete_this_article",           limit: 60
    t.string   "delete_article_confirmation",   limit: 60
    t.string   "naslov",                        limit: 60
    t.string   "sadrzaj",                       limit: 60
    t.string   "dodaj_sliku",                   limit: 60
    t.string   "back",                          limit: 60
    t.string   "show",                          limit: 60
    t.string   "izmeni_clanak",                 limit: 60
    t.string   "view_all_articles",             limit: 60
    t.string   "dodaj_asistenciju",             limit: 60
    t.string   "body_of_comment",               limit: 60
    t.string   "my_cms",                        limit: 60
    t.string   "dodaj_zuti_karton",             limit: 60
    t.string   "zuti_karton",                   limit: 60
    t.string   "unesite_svoje_ime",             limit: 60
    t.string   "naslov_poruke",                 limit: 60
    t.string   "sadrzaj_poruke",                limit: 60
    t.string   "kontaktirajte_nas_putem_maila",     limit: 60
    t.string   "lokacije_balona_u_kojima_se_igra",  limit: 60
    t.string   "home_page",                     limit: 60
    t.string   "enter_new_username",            limit: 60
    t.string   "enter_new_email",               limit: 60
    t.string   "enter_new_password",            limit: 60
    t.string   "posalji_poruku",                limit: 60
    t.string   "svi_korisnici",                 limit: 60
    t.string   "ime_i_prezime",                 limit: 60
    t.string   "moji_clanci",                   limit: 60
    t.string   "unesi_ime_delegata",            limit: 60
    t.string   "unesi_prezime_delegata",        limit: 60
    t.string   "unesi_lozinku_delegata",        limit: 60
    t.string   "unesi_registracioni_token",     limit: 60
    t.string   "delegate_edit",                 limit: 60
    t.string   "delegate_update",               limit: 60
    t.string   "all_delegates",                 limit: 60
    t.string   "registruj_delegata",            limit: 60
    t.string   "mecevi_koji_treba_da_se_odigraju",  limit: 60
    t.string   "odigrani_mecevi",                   limit: 60
    t.string   "molim_vas_sacekajte",           limit: 60
    t.string   "dodaj_faul",                    limit: 60
    t.string   "dodaj_odbranu",                 limit: 60
    t.string   "dodaj_gol",                     limit: 60
    t.string   "lige",                          limit: 60
    t.string   "sve_lige",                      limit: 60
    t.string   "kreiraj_ligu",                  limit: 60
    t.string   "dodaj_gostujuce_igrace",        limit: 60
    t.string   "dodaj_domace_igrace",           limit: 60
    t.string   "odbrana",                       limit: 60
    t.string   "odbrana_skraceno",              limit: 60
    t.string   "delegat",                       limit: 60
    t.string   "sudija",                        limit: 60
    t.string   "teren",                         limit: 60
    t.string   "startuj_mec",                   limit: 60
    t.string   "startuj_mec_potvrda",           limit: 60
    t.string   "kraj_I_poluvremena",            limit: 60
    t.string   "kraj_I_poluvremena_potvrda",    limit: 60
    t.string   "pocetak_II_poluvremena",        limit: 60
    t.string   "pocetak_II_poluvremena_potvrda",limit: 60
    t.string   "kraj_meca",                     limit: 60
    t.string   "kraj_meca_potvrda",             limit: 60
    t.string   "procenat_pobeda",               limit: 60
    t.string   "procenat_neresenih",            limit: 60
    t.string   "procenat_izgubljenih",          limit: 60
    t.string   "faulova",                       limit: 60
    t.string   "faulova_skraceno",              limit: 60
    t.string   "level",                         limit: 60
    t.string   "level_skraceno",                limit: 60
    t.string   "experience",                    limit: 60
    t.string   "experience_skraceno",           limit: 60
    t.string   "dodaj_crveni_karton",           limit: 60
    t.string   "sudije",                        limit: 60
    t.string   "slika_sudije",                  limit: 60
    t.string   "sve_sezone",                    limit: 60
    t.string   "kreiraj_sezonu",                limit: 60
    t.string   "prijavi_se",                    limit: 60
    t.string   "slika_igraca",                  limit: 60
    t.string   "ime",                           limit: 60
    t.string   "prezime",                       limit: 60
    t.string   "suteva",                        limit: 60
    t.string   "suteva_skraceno",               limit: 60
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "playerbadges_languages", force: :cascade do |t|
    t.string   "prvi_gol",                                  limit: 60
    t.string   "prva_asistencija",                          limit: 60
    t.string   "gol_u_prva_2_min",                          limit: 60
    t.string   "gol_iz_slobodnog",                          limit: 60
    t.string   "gol_iz_kornera",                            limit: 60
    t.string   "gol_glavom",                                limit: 60
    t.string   "gol_sa_svoje_polovine",                     limit: 60
    t.string   "tri_gola_na_utakmici",                      limit: 60
    t.string   "sest_golova_na_utakmici",                   limit: 60
    t.string   "deset_golova_na_utakmici",                  limit: 60
    t.string   "tri_asist_na_utakmici",                     limit: 60
    t.string   "sest_asist_na_utakmici",                    limit: 60
    t.string   "deset_asist_na_utakmici",                   limit: 60
    t.string   "tri_odbrane_na_utakmici",                   limit: 60
    t.string   "sest_odbrana_na_utakmici",                  limit: 60
    t.string   "deset_odbrana_na_utakmici",                 limit: 60
    t.string   "gol_asistencija",                           limit: 60
    t.string   "pet_gol_asistencija",                       limit: 60
    t.string   "tri_gola_na_tri_uzastopne",                 limit: 60
    t.string   "tri_gola_na_bilo_koje_tri",                 limit: 60
    t.string   "tri_asist_na_tri_uzastopne",                limit: 60
    t.string   "tri_asist_na_bilo_koje_tri",                limit: 60
    t.string   "tri_gola_na_osam_utakmica",                 limit: 60
    t.string   "odigrao_10_utakmica",                       limit: 60
    t.string   "po_jedna_liga_kup_prijatelj",               limit: 60
    t.string   "po_tri_liga_kup_prijatelj",                 limit: 60
    t.string   "po_jedan_gol_l_k_p",                        limit: 60
    t.string   "pet_prijateljskih",                         limit: 60
    t.string   "mvp_na_sedam_utakmica",                     limit: 60
    t.string   "mvp_kupa",                                  limit: 60
    t.string   "odigrane_sve_ligaske",                      limit: 60
  end

  create_table "teambadges_languages", force: :cascade do |t|
    t.string   "prva_pobeda",                               limit: 60
    t.string   "tri_uzastopne_pobede",                      limit: 60
    t.string   "sedam_uzastopnih_pobeda",                   limit: 60
    t.string   "sve_pobede_u_ligi",                         limit: 60
    t.string   "osvojena_liga",                             limit: 60
    t.string   "osvojen_kup",                               limit: 60
    t.string   "pet_prijateljskih_odirano",                 limit: 60
    t.string   "tri_bez_faula_i_kartona",                   limit: 60
    t.string   "tri_vezane_bez_primljenog",                 limit: 60
    t.string   "u_osam_kola_mvp",                           limit: 60
    t.string   "pobeda_sa_10_golova_razlike",               limit: 60
    t.string   "dato_vise_od_10_golova",                    limit: 60
    t.string   "dato_vise_od_20_golova",                    limit: 60
    t.string   "primljeno_0_golova",                        limit: 60
    t.string   "bez_faula_i_kartona",                       limit: 60
    t.string   "iz_2_u_1",                                  limit: 60
    t.string   "sutevi_golovi_sto_posto",                   limit: 60
    t.string   "tri_gola_tri_igraca",                       limit: 60
    t.string   "sedam_plus_asistencija",                    limit: 60
    t.string   "svi_igraci_dali_gol",                       limit: 60
  end

  create_table "league_pages", force: :cascade do |t|
    t.string "name", limit: 100, null: false
  end

  create_table "league_seasons", force: :cascade do |t|
    t.integer "league_id", limit: 8, null: false
    t.integer "season_id", limit: 8, null: false
  end

  add_index "league_seasons", ["league_id"], name: "league_id", using: :btree
  add_index "league_seasons", ["season_id"], name: "season_id", using: :btree

  create_table "leagues", force: :cascade do |t|
    t.string  "name",           limit: 100,                 null: false
    t.integer "rounds",         limit: 8,   default: 14
    t.boolean "is_fourplusone",             default: false
    t.boolean "is_fiveplusone",             default: false
  end

  create_table "mailboxer_conversation_opt_outs", force: :cascade do |t|
    t.integer "unsubscriber_id",   limit: 4
    t.string  "unsubscriber_type", limit: 255
    t.integer "conversation_id",   limit: 4
  end

  add_index "mailboxer_conversation_opt_outs", ["conversation_id"], name: "index_mailboxer_conversation_opt_outs_on_conversation_id", using: :btree
  add_index "mailboxer_conversation_opt_outs", ["unsubscriber_id", "unsubscriber_type"], name: "index_mailboxer_conversation_opt_outs_on_unsubscriber_id_type", using: :btree

  create_table "mailboxer_conversations", force: :cascade do |t|
    t.string   "subject",    limit: 255, default: ""
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  create_table "mailboxer_notifications", force: :cascade do |t|
    t.string   "type",                 limit: 255
    t.text     "body",                 limit: 65535
    t.string   "subject",              limit: 255,   default: ""
    t.integer  "sender_id",            limit: 4
    t.string   "sender_type",          limit: 255
    t.integer  "conversation_id",      limit: 4
    t.boolean  "draft",                              default: false
    t.string   "notification_code",    limit: 255
    t.integer  "notified_object_id",   limit: 4
    t.string   "notified_object_type", limit: 255
    t.string   "attachment",           limit: 255
    t.datetime "updated_at",                                         null: false
    t.datetime "created_at",                                         null: false
    t.boolean  "global",                             default: false
    t.datetime "expires"
  end

  add_index "mailboxer_notifications", ["conversation_id"], name: "index_mailboxer_notifications_on_conversation_id", using: :btree
  add_index "mailboxer_notifications", ["notified_object_id", "notified_object_type"], name: "index_mailboxer_notifications_on_notified_object_id_and_type", using: :btree
  add_index "mailboxer_notifications", ["sender_id", "sender_type"], name: "index_mailboxer_notifications_on_sender_id_and_sender_type", using: :btree
  add_index "mailboxer_notifications", ["type"], name: "index_mailboxer_notifications_on_type", using: :btree

  create_table "mailboxer_receipts", force: :cascade do |t|
    t.integer  "receiver_id",     limit: 4
    t.string   "receiver_type",   limit: 255
    t.integer  "notification_id", limit: 4,                   null: false
    t.boolean  "is_read",                     default: false
    t.boolean  "trashed",                     default: false
    t.boolean  "deleted",                     default: false
    t.string   "mailbox_type",    limit: 25
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
  end

  add_index "mailboxer_receipts", ["notification_id"], name: "index_mailboxer_receipts_on_notification_id", using: :btree
  add_index "mailboxer_receipts", ["receiver_id", "receiver_type"], name: "index_mailboxer_receipts_on_receiver_id_and_receiver_type", using: :btree

  create_table "match_players", force: :cascade do |t|
    t.integer  "player_season_id", limit: 8,                 null: false
    t.integer  "match_id",         limit: 8,                 null: false
    t.boolean  "is_home",                    default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "match_players", ["match_id"], name: "match_id", using: :btree
  add_index "match_players", ["player_season_id"], name: "player_season_id", using: :btree

  create_table "matches", force: :cascade do |t|
    t.integer  "home_goals",                  limit: 8, default: 0
    t.integer  "away_goals",                  limit: 8, default: 0
    t.integer  "home_assists",                limit: 8, default: 0
    t.integer  "away_assists",                limit: 8, default: 0
    t.integer  "home_fouls",                  limit: 8, default: 0
    t.integer  "away_fouls",                  limit: 8, default: 0
    t.integer  "home_yellow_cards",           limit: 8, default: 0
    t.integer  "away_yellow_cards",           limit: 8, default: 0
    t.integer  "home_red_cards",              limit: 8, default: 0
    t.integer  "away_red_cards",              limit: 8, default: 0
    t.integer  "home_shoots",                 limit: 8, default: 0
    t.integer  "away_shoots",                 limit: 8, default: 0
    t.integer  "home_goalkeeper_saves",       limit: 8, default: 0
    t.integer  "away_goalkeeper_saves",       limit: 8, default: 0
    t.integer  "round",                       limit: 8, default: 0
    t.integer  "match_started",               limit: 8, default: 0
    t.integer  "first_half_ended",            limit: 8, default: 0
    t.integer  "second_half_started",         limit: 8, default: 0
    t.integer  "match_finished",              limit: 8, default: 0
    t.datetime "match_date"
    t.integer  "home_goals_on_half",          limit: 8, default: 0
    t.integer  "away_goals_on_half",          limit: 8, default: 0
    t.boolean  "is_friendly",                           default: false
    t.boolean  "is_first_league",                       default: false
    t.boolean  "is_second_league",                      default: false
    t.boolean  "is_tournament",                         default: false
    t.boolean  "is_qualification_match",                default: false
    t.integer  "tournament_position",         limit: 3, default: 0
    t.boolean  "is_match_started",                      default: false
    t.boolean  "is_match_finished",                     default: false
    t.boolean  "is_home_win",                           default: false
    t.boolean  "is_home_draw",                          default: false
    t.boolean  "is_home_lose",                          default: false
    t.boolean  "is_home_team_players_chosen",           default: false
    t.boolean  "is_away_team_players_chosen",           default: false
    t.integer  "home_team_id",                limit: 8,                 null: false
    t.integer  "away_team_id",                limit: 8,                 null: false
    t.integer  "delegate_id",                 limit: 8
    t.integer  "referee_id",                  limit: 8
    t.integer  "stadion_id",                  limit: 8
    t.integer  "league_id",                   limit: 8
    t.integer  "tournament_id",               limit: 8
  end

  add_index "matches", ["away_team_id"], name: "away_team_id", using: :btree
  add_index "matches", ["delegate_id"], name: "delegate_id", using: :btree
  add_index "matches", ["home_team_id"], name: "home_team_id", using: :btree
  add_index "matches", ["league_id"], name: "league_id", using: :btree
  add_index "matches", ["referee_id"], name: "referee_id", using: :btree
  add_index "matches", ["stadion_id"], name: "stadion_id", using: :btree
  add_index "matches", ["tournament_id"], name: "tournament_id", using: :btree

  create_table "organisation_informations", force: :cascade do |t|
    t.string   "organisation_name",      limit: 200
    t.string   "moto",      limit: 100
    t.boolean  "show_introduction_section",           default: true
    t.string   "header_above_registration_link",      limit: 150
    t.text   "about_your_organisation_above_registration_link",      limit: 6000
    t.boolean  "show_6_boxes",           default: false
    t.string   "box_1",      limit: 400
    t.string   "box_2",      limit: 400
    t.string   "box_3",      limit: 400
    t.string   "box_4",      limit: 400
    t.string   "box_5",      limit: 400
    t.string   "box_6",      limit: 400
    t.boolean  "show_counter",           default: false
    t.integer  "number_of_teams",     limit: 8,   default: 0
    t.integer  "number_of_players",   limit: 8,   default: 0
    t.integer  "number_of_leagues",   limit: 3,   default: 0
    t.integer  "number_of_stadions",  limit: 3,   default: 0
    t.boolean  "show_map_on_welcome_page",               default: false
    t.boolean  "show_first_marketing_section",           default: false
    t.boolean  "show_second_marketing_section",          default: false
    t.string   "facebook_link",       limit: 100
    t.string   "twitter_link",        limit: 100
    t.string   "pinterest_link",      limit: 100
    t.string   "instagram_link",      limit: 100
    t.string   "email_1",      limit: 100
    t.string   "email_2",      limit: 100
    t.string   "email_3",      limit: 100
    t.string   "email_4",      limit: 100
    t.string   "phone_1",      limit: 100
    t.string   "phone_2",      limit: 100
    t.string   "avatar_file_name",    limit: 255
    t.string   "avatar_content_type", limit: 255
    t.integer  "avatar_file_size",    limit: 8
    t.datetime "avatar_updated_at"
  end

  create_table "own_goals", force: :cascade do |t|
    t.integer  "player_season_id", limit: 8,                 null: false
    t.integer  "match_id",         limit: 8,                 null: false
    t.boolean  "is_home",                    default: false
    t.integer  "created_at",       limit: 8
    t.datetime "updated_at"
  end

  add_index "own_goals", ["match_id"], name: "match_id", using: :btree
  add_index "own_goals", ["player_season_id"], name: "player_season_id", using: :btree

  create_table "player_badges", force: :cascade do |t|
    t.integer "prvi_gol",                    limit: 8, default: 0
    t.integer "prva_asistencija",            limit: 8, default: 0
    t.integer "gol_u_prva_2_min",            limit: 8, default: 0
    t.integer "gol_iz_slobodnog",            limit: 8, default: 0
    t.integer "gol_iz_kornera",              limit: 8, default: 0
    t.integer "gol_glavom",                  limit: 8, default: 0
    t.integer "gol_sa_svoje_polovine",       limit: 8, default: 0
    t.integer "tri_gola_na_utakmici",        limit: 8, default: 0
    t.integer "sest_golova_na_utakmici",     limit: 8, default: 0
    t.integer "deset_golova_na_utakmici",    limit: 8, default: 0
    t.integer "tri_asist_na_utakmici",       limit: 8, default: 0
    t.integer "sest_asist_na_utakmici",      limit: 8, default: 0
    t.integer "deset_asist_na_utakmici",     limit: 8, default: 0
    t.integer "tri_odbrane_na_utakmici",     limit: 8, default: 0
    t.integer "sest_odbrana_na_utakmici",    limit: 8, default: 0
    t.integer "deset_odbrana_na_utakmici",   limit: 8, default: 0
    t.integer "gol_asistencija",             limit: 8, default: 0
    t.integer "pet_gol_asistencija",         limit: 8, default: 0
    t.integer "tri_gola_na_tri_uzastopne",   limit: 8, default: 0
    t.integer "tri_gola_na_bilo_koje_tri",   limit: 8, default: 0
    t.integer "tri_asist_na_tri_uzastopne",  limit: 8, default: 0
    t.integer "tri_asist_na_bilo_koje_tri",  limit: 8, default: 0
    t.integer "tri_gola_na_osam_utakmica",   limit: 8, default: 0
    t.integer "odigrao_10_utakmica",         limit: 8, default: 0
    t.integer "po_jedna_liga_kup_prijatelj", limit: 8, default: 0
    t.integer "po_tri_liga_kup_prijatelj",   limit: 8, default: 0
    t.integer "po_jedan_gol_l_k_p",          limit: 8, default: 0
    t.integer "pet_prijateljskih",           limit: 8, default: 0
    t.integer "mvp_na_sedam_utakmica",       limit: 8, default: 0
    t.integer "mvp_kupa",                    limit: 8, default: 0
    t.integer "odigrane_sve_ligaske",        limit: 8, default: 0
    t.integer "player_id",                   limit: 8,             null: false
  end

  add_index "player_badges", ["player_id"], name: "player_id", using: :btree

  create_table "player_seasons", force: :cascade do |t|
    t.integer "matches_played",             limit: 8,  default: 0
    t.integer "matches_win",                limit: 8,  default: 0
    t.integer "matches_draw",               limit: 8,  default: 0
    t.integer "matches_lose",               limit: 8,  default: 0
    t.float   "win_ratio",                  limit: 24
    t.float   "draw_ratio",                 limit: 24
    t.float   "lose_ratio",                 limit: 24
    t.integer "goals",                      limit: 8,  default: 0
    t.integer "assists",                    limit: 8,  default: 0
    t.integer "fouls",                      limit: 8,  default: 0
    t.integer "yellow_cards",               limit: 8,  default: 0
    t.integer "red_cards",                  limit: 8,  default: 0
    t.integer "goalkeeper_saves",           limit: 8,  default: 0
    t.integer "own_goals",                  limit: 8,  default: 0
    t.integer "position",                   limit: 8,  default: 0
    t.integer "expirience",                 limit: 8,  default: 0
    t.integer "level",                      limit: 8,  default: 0
    t.integer "goals_in_chain",             limit: 8,  default: 0
    t.integer "assists_in_chain",           limit: 8,  default: 0
    t.integer "gk_saves_in_chain",          limit: 8,  default: 0
    t.integer "three_goals_in_chain",       limit: 8,  default: 0
    t.integer "three_assists_in_chain",     limit: 8,  default: 0
    t.integer "league_matches_played",      limit: 8,  default: 0
    t.integer "friendly_matches_played",    limit: 8,  default: 0
    t.integer "tournament_matches_played",  limit: 8,  default: 0
    t.integer "goals_on_league",            limit: 8,  default: 0
    t.integer "goals_on_friendly",          limit: 8,  default: 0
    t.integer "goals_on_tournament",        limit: 8,  default: 0
    t.integer "broj_dresa",                 limit: 2
    t.integer "mvp_on_matches",             limit: 8,  default: 0
    t.integer "mvp_on_tournament_in_chain", limit: 8,  default: 0
    t.boolean "is_captain",                            default: false
    t.boolean "is_suspended",                          default: false
    t.integer "team_season_id",             limit: 8
    t.integer "season_id",                  limit: 8,                  null: false
    t.integer "player_id",                  limit: 8,                  null: false
  end

  add_index "player_seasons", ["player_id"], name: "player_id", using: :btree
  add_index "player_seasons", ["season_id"], name: "season_id", using: :btree
  add_index "player_seasons", ["team_season_id"], name: "team_season_id", using: :btree

  create_table "players", force: :cascade do |t|
    t.string   "name",                      limit: 50
    t.string   "surname",                   limit: 50
    t.string   "JMBG",                      limit: 20
    t.date     "birthdate"
    t.integer  "player_registration_token", limit: 8
    t.integer  "procenat_levela",           limit: 3
    t.string   "avatar_file_name",          limit: 255
    t.string   "avatar_content_type",       limit: 255
    t.integer  "avatar_file_size",          limit: 8
    t.datetime "avatar_updated_at"
    t.boolean  "is_approved"
    t.boolean  "is_goalkeeper"
    t.integer  "team_id",                   limit: 8
  end

  add_index "players", ["team_id"], name: "team_id", using: :btree

  create_table "red_cards", force: :cascade do |t|
    t.integer  "player_season_id", limit: 8,                 null: false
    t.integer  "match_id",         limit: 8,                 null: false
    t.boolean  "is_home",                    default: false
    t.integer  "created_at",       limit: 8
    t.datetime "updated_at"
  end

  add_index "red_cards", ["match_id"], name: "match_id", using: :btree
  add_index "red_cards", ["player_season_id"], name: "player_season_id", using: :btree

  create_table "referees", force: :cascade do |t|
    t.string   "name",                limit: 50
    t.string   "surname",             limit: 50
    t.string   "JMBG",                limit: 20
    t.date     "birthdate"
    t.string   "avatar_file_name",    limit: 255
    t.string   "avatar_content_type", limit: 255
    t.integer  "avatar_file_size",    limit: 8
    t.datetime "avatar_updated_at"
  end

  create_table "seasons", force: :cascade do |t|
    t.string  "name",                        limit: 100, null: false
    t.integer "registration_token",          limit: 8
    t.integer "delegate_registration_token", limit: 8
    t.integer "player_registration_token",   limit: 8
  end

  create_table "shoots", force: :cascade do |t|
    t.integer  "team_season_id", limit: 8,                 null: false
    t.integer  "match_id",       limit: 8,                 null: false
    t.boolean  "is_home",                  default: false
    t.integer  "created_at",     limit: 8
    t.datetime "updated_at"
  end

  add_index "shoots", ["match_id"], name: "match_id", using: :btree
  add_index "shoots", ["team_season_id"], name: "team_season_id", using: :btree

  create_table "stadions", force: :cascade do |t|
    t.string   "name",                limit: 50
    t.string   "address",             limit: 50
    t.string   "url",                 limit: 50
    t.string   "avatar_file_name",    limit: 255
    t.string   "avatar_content_type", limit: 255
    t.integer  "avatar_file_size",    limit: 8
    t.datetime "avatar_updated_at"
  end

  create_table "team_badges", force: :cascade do |t|
    t.boolean "prva_pobeda",                           default: false
    t.integer "tri_uzastopne_pobede",        limit: 8, default: 0
    t.integer "sedam_uzastopnih_pobeda",     limit: 8, default: 0
    t.integer "sve_pobede_u_ligi",           limit: 8, default: 0
    t.integer "osvojena_liga",               limit: 8, default: 0
    t.integer "osvojen_kup",                 limit: 8, default: 0
    t.integer "pet_prijateljskih_odirano",   limit: 8, default: 0
    t.integer "tri_bez_faula_i_kartona",     limit: 8, default: 0
    t.integer "tri_vezane_bez_primljenog",   limit: 8, default: 0
    t.integer "u_osam_kola_mvp",             limit: 8, default: 0
    t.integer "pobeda_sa_10_golova_razlike", limit: 8, default: 0
    t.integer "dato_vise_od_10_golova",      limit: 8, default: 0
    t.integer "dato_vise_od_20_golova",      limit: 8, default: 0
    t.integer "primljeno_0_golova",          limit: 8, default: 0
    t.integer "bez_faula_i_kartona",         limit: 8, default: 0
    t.integer "iz_2_u_1",                    limit: 8, default: 0
    t.integer "sutevi_golovi_sto_posto",     limit: 8, default: 0
    t.integer "tri_gola_tri_igraca",         limit: 8, default: 0
    t.integer "sedam_plus_asistencija",      limit: 8, default: 0
    t.integer "svi_igraci_dali_gol",         limit: 8, default: 0
    t.integer "team_id",                     limit: 8,                 null: false
  end

  add_index "team_badges", ["team_id"], name: "team_id", using: :btree

  create_table "team_players", force: :cascade do |t|
    t.string   "avatar_file_name",    limit: 255
    t.string   "avatar_content_type", limit: 255
    t.integer  "avatar_file_size",    limit: 8
    t.datetime "avatar_updated_at"
    t.integer  "team_id",             limit: 8,   null: false
  end

  add_index "team_players", ["team_id"], name: "team_id", using: :btree

  create_table "team_seasons", force: :cascade do |t|
    t.integer "matches_played",               limit: 8,  default: 0
    t.integer "matches_win",                  limit: 8,  default: 0
    t.integer "matches_draw",                 limit: 8,  default: 0
    t.integer "matches_lose",                 limit: 8,  default: 0
    t.float   "win_ratio",                    limit: 24
    t.float   "draw_ratio",                   limit: 24
    t.float   "lose_ratio",                   limit: 24
    t.integer "total_goals",                  limit: 8,  default: 0
    t.integer "goals_scored",                 limit: 8,  default: 0
    t.integer "goals_received",               limit: 8,  default: 0
    t.integer "assists",                      limit: 8,  default: 0
    t.integer "shoots",                       limit: 8,  default: 0
    t.float   "agressive_ratio",              limit: 24
    t.integer "points",                       limit: 8,  default: 0
    t.integer "total_red_cards",              limit: 8,  default: 0
    t.integer "total_yellow_cards",           limit: 8,  default: 0
    t.integer "position",                     limit: 8
    t.integer "expirience",                   limit: 8,  default: 0
    t.integer "level",                        limit: 8,  default: 0
    t.integer "wins_in_chain",                limit: 8,  default: 0
    t.integer "wins_on_league",               limit: 8,  default: 0
    t.boolean "osvojena_liga",                           default: false
    t.boolean "osvojen_kup",                             default: false
    t.integer "friendly_matches_played",      limit: 8,  default: 0
    t.integer "fair_play_matches_played",     limit: 8,  default: 0
    t.integer "bez_primljenog_gola_in_chain", limit: 8,  default: 0
    t.integer "mvp_on_rounds",                limit: 8,  default: 0
    t.integer "more_than_ten_goals_win",      limit: 8,  default: 0
    t.integer "fair_play_matches",            limit: 8,  default: 0
    t.integer "from_2_to_1_win",              limit: 8,  default: 0
    t.integer "total_shoots_goals_ratio",     limit: 8,  default: 0
    t.integer "players_hat_trick",            limit: 8,  default: 0
    t.integer "players_scored_goal_on_match", limit: 8,  default: 0
    t.integer "league_season_id",             limit: 8
    t.integer "season_id",                    limit: 8,                  null: false
    t.integer "team_id",                      limit: 8,                  null: false
  end

  add_index "team_seasons", ["league_season_id"], name: "league_season_id", using: :btree
  add_index "team_seasons", ["season_id"], name: "season_id", using: :btree
  add_index "team_seasons", ["team_id"], name: "team_id", using: :btree

  create_table "teams", force: :cascade do |t|
    t.string   "name",                limit: 50,                null: false
    t.integer  "registration_token",  limit: 8
    t.string   "fb_url",              limit: 255, default: "0"
    t.string   "avatar_file_name",    limit: 255
    t.string   "avatar_content_type", limit: 255
    t.integer  "avatar_file_size",    limit: 8
    t.datetime "avatar_updated_at"
    t.integer  "tournament_id",       limit: 8
    t.integer  "league_id",           limit: 8
  end

  add_index "teams", ["league_id"], name: "league_id", using: :btree
  add_index "teams", ["tournament_id"], name: "tournament_id", using: :btree

  create_table "tournaments", force: :cascade do |t|
    t.string  "name",           limit: 100,                 null: false
    t.integer "rounds",         limit: 8,   default: 14
    t.boolean "is_fourplusone",             default: false
    t.boolean "is_fiveplusone",             default: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "username",               limit: 50
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 8,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.string   "confirmation_token",     limit: 255
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email",      limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.string   "player_id",              limit: 5
    t.string   "delegate_id",            limit: 5
    t.string   "admin_id",               limit: 5
  end

  add_index "users", ["admin_id"], name: "admin_id", using: :btree
  add_index "users", ["delegate_id"], name: "delegate_id", using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["player_id"], name: "player_id", using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "votes", force: :cascade do |t|
    t.integer  "votable_id",   limit: 8
    t.string   "votable_type", limit: 255
    t.integer  "voter_id",     limit: 8
    t.string   "voter_type",   limit: 255
    t.boolean  "vote_flag"
    t.string   "vote_scope",   limit: 255
    t.integer  "vote_weight",  limit: 8
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "votes", ["votable_id", "votable_type", "vote_scope"], name: "index_votes_on_votable_id_and_votable_type_and_vote_scope", using: :btree
  add_index "votes", ["voter_id", "voter_type", "vote_scope"], name: "index_votes_on_voter_id_and_voter_type_and_vote_scope", using: :btree

  create_table "yellow_cards", force: :cascade do |t|
    t.integer  "player_season_id", limit: 8,                 null: false
    t.integer  "match_id",         limit: 8,                 null: false
    t.boolean  "is_home",                    default: false
    t.integer  "created_at",       limit: 8
    t.datetime "updated_at"
  end

  add_index "yellow_cards", ["match_id"], name: "match_id", using: :btree
  add_index "yellow_cards", ["player_season_id"], name: "player_season_id", using: :btree

end
