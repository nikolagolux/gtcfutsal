require 'test_helper'

class GoalkeeperSavesControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get show" do
    get :show
    assert_response :success
  end

  test "should get new_home" do
    get :new_home
    assert_response :success
  end

  test "should get new_away" do
    get :new_away
    assert_response :success
  end

end
