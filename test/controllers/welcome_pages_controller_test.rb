require 'test_helper'

class WelcomePagesControllerTest < ActionController::TestCase
  test "should get contact" do
    get :contact
    assert_response :success
  end

  test "should get news" do
    get :news
    assert_response :success
  end

  test "should get rules" do
    get :rules
    assert_response :success
  end

end
